from pathlib import Path
import setuptools

long_description = Path('README.md').read_text()

setuptools.setup(
	name = 'funcli',
	version = '0.7.1',
	author = "valtron",
	description = "Automatically generate a simple CLI.",
	long_description = long_description,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/funcli',
	packages = ['funcli'],
	package_data = { 'funcli': ['py.typed'] },
	python_requires = '>=3.6',
	install_requires = ['docstring-parser', 'typing-extensions'],
	setup_requires = ['pytest-runner'],
	tests_require = ['pytest', 'pytest-cov'],
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Programming Language :: Python :: 3.8',
		'Programming Language :: Python :: 3.9',
		'Programming Language :: Python :: 3.10',
		'Programming Language :: Python :: 3.11',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)
