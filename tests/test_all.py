from typing import Sequence, Iterable, List, Set, Tuple, Optional
from typing_extensions import Final
import pytest
import funcli

def test_spec_func():
	def f(a): return a
	ret = funcli.run(f, ['a'])
	assert ret == 'a'

def test_spec_seq():
	def f(a): return 'f', a
	def g(a): return 'g', a
	fs = { f, g }
	ret_f = funcli.run(fs, ['f', 'a'])
	assert ret_f == ('f', 'a')
	ret_g = funcli.run(fs, ['g', 'a'])
	assert ret_g == ('g', 'a')

def test_spec_map():
	def f(a): return 'f', a
	def g(a): return 'g', a
	fs = { 'fx': f, 'gx': g }
	ret_f = funcli.run(fs, ['fx', 'a'])
	assert ret_f == ('f', 'a')
	ret_g = funcli.run(fs, ['gx', 'a'])
	assert ret_g == ('g', 'a')

def test_spec_complex():
	def f(a): return 'f', a
	def h(a): return 'h', a
	def k(a): return 'k', a
	fs = { 'f': f, 'g': { h, k } }
	ret_f = funcli.run(fs, ['f', 'a'])
	assert ret_f == ('f', 'a')
	ret_h = funcli.run(fs, ['g', 'h', 'a'])
	assert ret_h == ('h', 'a')
	ret_k = funcli.run(fs, ['g', 'k', 'a'])
	assert ret_k == ('k', 'a')

def test_long_name():
	def f(*, foo): return foo
	ret = funcli.run(f, ['--foo=bar'])
	assert ret == 'bar'

@pytest.mark.skip(reason = "argparse doesn't support kwargs")
def test_kwargs_type():
	def f(**kw: int): return kw
	ret = funcli.run(f, ['--foo=1'])
	assert ret == { 'foo': 1 }

def test_conversions():
	def f(a: int, b: float, c: str, d: bool, e):
		return (a, b, c, d, e)
	ret_1 = funcli.run(f, ['1', '2.3', 'abc', 'True', 'asdjk'])
	assert ret_1 == (1, 2.3, 'abc', True, 'asdjk')
	ret_2 = funcli.run(f, ['0', '2', 'abc', 'False', '0'])
	assert ret_2 == (0, 2.0, 'abc', False, '0')

def test_defaults():
	def f(*, a: int = 2):
		return a
	ret_1 = funcli.run(f, ['--a=1'])
	assert ret_1 == 1
	ret_2 = funcli.run(f, [])
	assert ret_2 == 2

def test_string_annot():
	def f(a: 'TYP'): return a
	ret = funcli.run(f, ['1'])
	assert ret == 1

def test_bool():
	def f(a: bool = False): return a
	ret_1 = funcli.run(f, ['--a'])
	assert ret_1 == True
	
	def f(*, a: bool = False): return a
	ret_3 = funcli.run(f, ['--a'])
	assert ret_3 == True
	
	def f(a: bool): return a
	ret_4 = funcli.run(f, ['True'])
	assert ret_4 == True

def test_seq():
	def f(*, a: List[int] = []): return a
	ret_1 = funcli.run(f, ['--a', '1', '2', '3'])
	assert ret_1 == [1, 2, 3]
	
	def f(*, a: Set[int] = []): return a
	ret_2 = funcli.run(f, ['--a', '1', '2', '3'])
	assert ret_2 == { 1, 2, 3 }
	
	def f(*, a: Tuple[int, ...] = []): return a
	ret_3 = funcli.run(f, ['--a', '1', '2', '3'])
	assert ret_3 == (1, 2, 3)
	
	def f(*, a: Sequence[int] = []): return a
	ret_4 = funcli.run(f, ['--a', '1', '2', '3'])
	assert list(ret_4) == [1, 2, 3]
	
	def f(*, a: Iterable[int] = []): return a
	ret_5 = funcli.run(f, ['--a', '1', '2', '3'])
	assert list(ret_5) == [1, 2, 3]

def test_optional():
	def f(a: Optional[int] = 1): return a
	ret_1 = funcli.run(f, ['--a', '2'])
	assert ret_1 == 2
	
	def f(a: Optional[int] = None): return a
	ret_2 = funcli.run(f, [])
	assert ret_2 is None
	
	def f(a: Optional[int] = 1): return a
	ret_3 = funcli.run(f, [])
	assert ret_3 == 1

def test_seq_optional():
	def f(*, a: Optional[Set[int]] = None): return a
	ret_1 = funcli.run(f, ['--a', '1', '2', '3'])
	assert ret_1 == { 1, 2, 3 }
	
	def f(*, a: Optional[Set[int]] = None): return a
	ret_2 = funcli.run(f, ['--a'])
	assert ret_2 == set()
	
	def f(*, a: Optional[Set[int]] = None): return a
	ret_3 = funcli.run(f, [])
	assert ret_3 is None

def test_thing(capsys):
	def f(): pass
	
	with pytest.raises(SystemExit):
		funcli.run({ f }, ['--help'])
	captured = capsys.readouterr()
	assert "usage:" in captured.out
	
	with pytest.raises(SystemExit):
		funcli.run({ f }, [])
	captured = capsys.readouterr()
	assert "usage:" in captured.out

def test_docstring(capsys):
	def f(a, b):
		"""
			This is the documentation for the function.
			
			It spans several lines.
			
			:param a: description for a
			:param b: description for b
		"""
	
	with pytest.raises(SystemExit):
		funcli.run(f, ['--help'])
	captured = capsys.readouterr()
	
	assert "This is the doc" in captured.out
	assert "It spans" in captured.out
	assert "description for a" in captured.out
	assert "description for b" in captured.out

def test_final():
	def f(a: Final[int]): return a
	ret_1 = funcli.run(f, ['1'])
	assert ret_1 == 1
	
	def f(a: Final[Optional[int]]): return a
	ret_3 = funcli.run(f, ['3'])
	assert ret_3 == 3

def test_underscore():
	def f(a_b: int = 0): return a_b
	ret_1 = funcli.run(f, ['--a-b', '1'])
	assert ret_1 == 1
	
	def f(*pos_args: int): return pos_args
	ret_2 = funcli.run(f, ['1', '2'])
	assert ret_2 == (1, 2)

@pytest.mark.skip(reason = "argparse doesn't support kwargs")
def test_underscore_kwargs():
	def f(*pos_args: int, **kw_args: int): return (pos_args, kw_args)
	ret_1 = funcli.run(f, ['1', '2', '--foo=3', '--bar-baz', '4', '--abc_def', '5'])
	assert ret_1 == ((1, 2), { 'foo': 3, 'bar_baz': 4, 'abc_def': 5 })

def test_underscore_no_dash():
	def f(a_b: int = 0): return a_b
	ret_1 = funcli.run(f, ['--a_b', '1'], dash = False)
	assert ret_1 == 1
	
	def f(*pos_args: int): return pos_args
	ret_2 = funcli.run(f, ['1', '2'], dash = False)
	assert ret_2 == (1, 2)

@pytest.mark.skip(reason = "argparse doesn't support kwargs")
def test_underscore_kwargs_no_dash():
	def f(*pos_args: int, **kw_args: int): return (pos_args, kw_args)
	ret_1 = funcli.run(f, ['1', '2', '--foo=3', '--bar-baz', '4', '--abc_def', '5'], dash = False)
	assert ret_1 == ((1, 2), { 'foo': 3, 'bar_baz': 4, 'abc_def': 5 })

def test_underscore_required_positional():
	def f(pos_arg: int): return pos_arg
	ret_1 = funcli.run(f, ['1'])
	assert ret_1 == 1

def test_main():
	with pytest.raises(SystemExit) as ex:
		funcli.main()
	assert ex.value.code == 123

def main():
	return 123

TYP = int
